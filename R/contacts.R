# Code I used to create the shielded group in data/part_shield.qs
# library(tidyverse)
# qs::qread("~/Downloads/tmp/part_20220302.qs") -> dat
# # Duplicated columns
# # duplicated(colnames(dat)) |> which()
# # dat[,c(7,11)]
# # dat[,c(8,218)]
# # dat[,c(9,219)]
# dat[,-11] -> dat
# dat[,-217] -> dat
# dat[,-216] -> dat
# start2 = as.Date("2020-08-09")
# dat |>
#     dplyr::mutate(part_id = stringr::str_replace(part_wave_uid, "uk_", "")) |>
#   dplyr::select(part_high_risk, part_high_risk_v2, part_att_serious, part_age_group, date, part_id) %>% #Keep NA as "no shield"
#   ##study end date (NB: sum(is.na(dp$date)=)0))
#   ##convert factor to character
#   mutate_if(is.factor, as.character) %>% #  glimpse()
#   ##define shielding flag combining three variables
#   ###more stringent
#     dplyr::mutate(part_high_risk_v3 = case_when(date<start2 ~ "yes", TRUE ~ part_high_risk_v2)) %>% #creates non-restricting part_high_risk_v2 before it exists
#     dplyr::mutate(shield1 = (part_high_risk_v3=="yes" & part_high_risk=="yes" & part_att_serious %in% c("Strongly agree"))) |>
# #   mutate(shield1 = (part_high_risk_v2=="yes" & part_high_risk=="yes" & part_att_serious %in% c("Strongly agree")))  %>%
# #   mutate(shield1 = case_when(is.na(shield1) ~ FALSE, TRUE ~ shield1)) %>% #NA in variables mean "not shield"
# #   ##less stringent
# #   mutate(shield2 = (part_high_risk_v2=="yes" & part_high_risk=="yes" & part_att_serious %in% c("Strongly agree", "Tend to agree")))  %>%
# #   mutate(shield2 = case_when(is.na(shield2) ~ FALSE, TRUE ~ shield2)) %>% #NA in variables mean "not shield"
# #   ##***current shielding variable
#   mutate(shield = shield1) |>
#   dplyr::select(part_id, shield) -> dat1
# dat1
# qs::qsave(dat1, "data/part_shield.qs")
#' Take a survey and return an object suitable for contact inference
#'
#' @param survey_lst A list containing the survey. Layout it the same as the survey lists in the socialmixr package
#' @param country The country to use. Country names tend to be survey specific and are assumed to be in survey_lst$participants$country
#' @param age_limits The age limits to use. Should be a list of upper ages for each category. Age groups will be non inclusive the upper limit
#' @return A matrix with the data summarised suitable for contact inference
#' @export
prepare_contact_survey_for_inference <- function(survey_lst, country, age_limits) {
  # age_limits <- c(0,5,25,45,65)
  f_limits <- function(age) cut(age, c(age_limits, Inf), right = F)
  survey_lst$participants |>
    dplyr::filter(`country` == country) |>
    dplyr::mutate(age_group = f_limits(part_age)) |>
    dplyr::select(part_id, part_age, age_group) -> dat0

  survey_lst$contacts |>
    dplyr::mutate(
      age = (cnt_age_est_max + cnt_age_est_min) / 2,
      age = dplyr::coalesce(cnt_age_exact, age)
    ) |>
    dplyr::mutate(cnt_age_group = f_limits(age)) |>
    dplyr::inner_join(dat0) -> dat1

  dat1 |>
    dplyr::filter(!is.na(age_group), !is.na(cnt_age_group)) |>
    dplyr::group_by(part_id, age_group, cnt_age_group) |>
    dplyr::summarise(value = dplyr::n()) |>
    dplyr::ungroup() -> dat2
  dat2 |>
    dplyr::select(part_id, cnt_age_group, value) |>
    # Complete with missing zeros
    tidyr::complete(part_id, cnt_age_group, fill = list(value = 0)) |>
    dplyr::arrange(part_id, cnt_age_group) |>
    dplyr::left_join(dat2 |> dplyr::select(part_id, age_group) |> dplyr::distinct()) |>
    dplyr::select(-part_id) -> dat

  dat |>
    dplyr::mutate(age_group = as.numeric(age_group)) |>
    dplyr::mutate(cnt_age_group = as.numeric(cnt_age_group)) |>
    dplyr::group_by(age_group, cnt_age_group, value) |>
    dplyr::summarise(weight = dplyr::n()) |>
    dplyr::ungroup() |>
    dplyr::arrange(age_group, cnt_age_group, value, weight) |>
    dplyr::arrange(age_group, cnt_age_group, value) |>
    as.matrix() -> m_dat

  m_dat
}

ms_prepare_contact_survey <- function(survey_lst, country, age_limits) {
  # age_limits <- c(0,5,25,45,65)
  f_limits <- function(age) cut(age, c(age_limits, Inf), right = F)
  survey_lst$participants |>
    dplyr::filter(`country` == country) |>
    dplyr::mutate(age_group = f_limits(part_age)) |>
    dplyr::select(part_id, part_age, age_group) -> dat0

  survey_lst$contacts |>
    dplyr::mutate(
      age = (cnt_age_est_max + cnt_age_est_min) / 2,
      age = dplyr::coalesce(cnt_age_exact, age)
    ) |>
    dplyr::mutate(cnt_age_group = f_limits(age)) |>
    dplyr::inner_join(dat0) -> dat1

  dat1 |>
    dplyr::filter(!is.na(age_group), !is.na(cnt_age_group)) |>
    dplyr::group_by(part_id, age_group, cnt_age_group) |>
    dplyr::summarise(value = dplyr::n()) |>
    dplyr::ungroup() -> dat2
  dat2 |>
    dplyr::select(part_id, cnt_age_group, value) |>
    # Complete with missing zeros
    tidyr::complete(part_id, cnt_age_group, fill = list(value = 0)) |>
    dplyr::arrange(part_id, cnt_age_group) |>
    dplyr::left_join(dat2 |> dplyr::select(part_id, age_group) |> dplyr::distinct()) -> dat3

  stopifnot(length(age_limits) == 9)

  dat3 |>
    dplyr::mutate(age_group = as.numeric(age_group)) |>
    dplyr::mutate(cnt_age_group = as.numeric(cnt_age_group)) |>
    tidyr::pivot_wider(names_from = cnt_age_group, values_from = value) |>
    dplyr::select(-part_id) |>
    dplyr::summarise(n = dplyr::n(), .by = c(age_group, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`)) |>
    dplyr::select(age_group, n, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`) |>
    as.matrix()
}



prepare_contact_survey_with_shielding <- function(survey_lst, country, age_limits, shielders) {
  # Shielders = T will only give the shielders
  # Shielders = F will only give the non shielders
  # library(targets)
  # tar_load(survey_dat)
  # tar_load(parameters)
  # survey_lst <- survey_dat$survey_lst
  # age_limits <- parameters$age_limits
  # shielders <- T
  # country <- "uk"
  # stop()
  f_limits <- function(age) cut(age, c(age_limits, Inf), right = F)
  survey_lst$participants |>
    dplyr::filter(`country` == country, shield == shielders) |>
    dplyr::mutate(age_group = f_limits(part_age)) |>
    dplyr::select(part_id, part_age, age_group) -> dat0
  # as.numeric(dat0$shield)
  survey_lst$contacts |>
    dplyr::mutate(
      age = (cnt_age_est_max + cnt_age_est_min) / 2,
      age = dplyr::coalesce(cnt_age_exact, age)
    ) |>
    dplyr::mutate(cnt_age_group = f_limits(age)) |>
    dplyr::inner_join(dat0) -> dat1

  dat1 |>
    dplyr::filter(!is.na(age_group), !is.na(cnt_age_group)) |>
    dplyr::group_by(part_id, age_group, cnt_age_group) |>
    dplyr::summarise(value = dplyr::n()) |>
    dplyr::ungroup() -> dat2

  dat2 |>
    dplyr::select(part_id, cnt_age_group, value) |>
    # Complete with missing zeros
    tidyr::complete(part_id, cnt_age_group, fill = list(value = 0)) |>
    dplyr::arrange(part_id, cnt_age_group) |>
    dplyr::left_join(dat2 |> dplyr::select(part_id, age_group) |> dplyr::distinct()) |>
    dplyr::select(-part_id) -> dat

  dat |>
    dplyr::mutate(age_group = as.numeric(age_group)) |>
    dplyr::mutate(cnt_age_group = as.numeric(cnt_age_group)) |>
    dplyr::group_by(age_group, cnt_age_group, value) |>
    dplyr::summarise(weight = dplyr::n()) |>
    dplyr::ungroup() |>
    dplyr::arrange(age_group, cnt_age_group, value, weight) |>
    dplyr::arrange(age_group, cnt_age_group, value) |>
    as.matrix() -> m_dat

  m_dat
}

get_comix_contact_data <- function(shield_fn) {
  # stop()
  # socialmixr::list_surveys() |>
  #  dplyr::filter(title == "CoMix social contact data (UK)") |>
  #  dplyr::pull(url) -> doi

  # My implementation
  doi <- "https://doi.org/10.5281/zenodo.6542524"
  # Takes forever because it will try to join on X column, instead download by hadn
  # comix_orig <- socialmixr::get_survey(doi)
  dir.create("/tmp/comix")
  files <- socialmixr::download_survey(doi, "/tmp/comix")
  files |>
    purrr::keep(function(fn) stringr::str_detect(fn, "(hh|sday)")) |>
    purrr::walk(function(fn) {
      readr::read_csv(fn) |>
        dplyr::select(-X) -> dat0
      write.csv(dat0, fn, row.names = F)
    })
  # part_age is specified as midpoint of range in socialmixr
  # revert back to range and then sample uniformly
  fn <- "/tmp/comix/comix_uk_participant_common.csv"
  readr::read_csv(fn) |>
    dplyr::mutate(est = stringr::str_extract_all(part_age, "\\d+")) |>
    tidyr::separate(est, into = c("a", "mn", "mx")) |>
    dplyr::mutate(part_age = round(runif(dplyr::n(), as.numeric(mn), as.numeric(mx)))) |>
    dplyr::filter(is.finite(part_age)) -> dat
  write.csv(dat, fn, row.names = F)
  fn <- "/tmp/comix/comix_uk_contact_common.csv"
  readr::read_csv(fn) |>
    dplyr::mutate(cnt_age_exact = dplyr::coalesce(cnt_age_exact, round(runif(dplyr::n(), cnt_age_est_min, cnt_age_est_max)))) |>
    dplyr::filter(is.finite(cnt_age_exact)) -> dat
  write.csv(dat, fn, row.names = F)

  comix_survey <- socialmixr::load_survey(files)
  shield_dat <- qs::qread(shield_fn)
  comix_survey$participants <- comix_survey$participants |>
    dplyr::left_join(shield_dat, by = "part_id")
  unlink("/tmp/comix", recursive = T)
  # comix_survey <- socialmixr::survey(comix$participants, comix$contacts)
  comix_survey
}

geom_matrix <- function(contact_lst, mx_contacts) {
  contact_lst$matrix %>%
    dplyr::as_tibble() |>
    dplyr::mutate(AgeGroup = dplyr::row_number()) |>
    tidyr::gather(AgeGroup2, value, -AgeGroup) -> plot_df

  ggplot(data = plot_df) +
    geom_tile(aes(x = AgeGroup2, y = AgeGroup, fill = value), alpha = 0.7) +
    geom_text(aes(x = AgeGroup2, y = AgeGroup, label = round(value, digit = 2), size = value)) +
    scale_size(range = c(2, 3)) +
    theme(legend.position = "bottom", legend.title = element_blank()) +
    # scale_fill_gradient(low = "white", high = "black") +
    scale_fill_distiller(palette = "Spectral", limits = c(0, mx_contacts)) +
    # scale_fill_gradient(low = scales::muted("blue"), high = scales::muted("red")) +
    # scale_fill_gradientn(colors = rainbow(7)) +
    theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank()) +
    ylab("Age of contact") +
    xlab("Age of participant") +
    theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
    theme(axis.text.y = element_text(angle = 45))
}
