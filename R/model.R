ms_comix_to_data_lst <- function(survey_dat, parameters) {
  # library(targets)
  # tar_load(survey_dat)
  # source("R/contacts.R")
  # t_id <- 1
  # stop()
  survey_lst <- survey_dat$survey_lst
  time_dat <- survey_dat$time_dat

  time_dat$time_id |>
    unique() |>
    purrr::map(function(t_id) {
      lst <- survey_lst
      lst$participants |>
        dplyr::inner_join(time_dat) |>
        dplyr::filter(time_id == t_id) -> lst$participants

      list(time_id = t_id, contact_survey = ms_prepare_contact_survey(lst, "uk", parameters$age_limits))
    })
}

comix_to_data_lst <- function(survey_dat, parameters, short) {
  # library(targets)
  # tar_load(survey_dat)
  # source("R/contacts.R")
  # short <- T
  # stop()
  survey_lst <- survey_dat$survey_lst
  time_dat <- survey_dat$time_dat
  time_dat <- if (short) {
    time_dat |> dplyr::filter(time_id <= 26)
  } else {
    time_dat
  }

  time_dat$time_id |>
    unique() |>
    purrr::map(function(t_id) {
      lst <- survey_lst
      lst$participants |>
        dplyr::inner_join(time_dat) |>
        dplyr::filter(time_id == t_id) -> lst$participants

      # Calc proportion shielding, used in the explanatory_variables
      lst$participants -> dat0
      ageutils::cut_ages(dat0$part_age, breaks = parameters$age_limits) |>
        dplyr::bind_cols(dat0) |>
        # dplyr::summarise(n = dplyr::n(), .by = c(shield, interval)) |>
        dplyr::summarise(n = dplyr::n(), .by = c(shield)) |>
        tidyr::pivot_wider(names_from = shield, values_from = n) -> dat2

      dat1 <- if ("TRUE" %in% colnames(dat2)) {
        # dplyr::arrange(interval) |>
        dat2 |>
          dplyr::mutate(`TRUE` = dplyr::coalesce(`TRUE`, 0)) |>
          dplyr::mutate(shield_prop = `TRUE` / (`TRUE` + `FALSE`))
      } else {
        dat2 |>
          dplyr::mutate(shield_prop = 0)
      }

      list(shield_prop = dat1$shield_prop, time_id = t_id) -> r_lst

      r_lst$contact_survey <- prepare_contact_survey_with_shielding(lst, "uk", parameters$age_limits, shielders = FALSE)
      r_lst$contact_survey_shielding <- prepare_contact_survey_with_shielding(lst, "uk", parameters$age_limits, shielders = TRUE)
      r_lst
    }) -> data_lst

  # Collect data for proportion of shielders by age group
  data_lst |>
    purrr::map(function(lst0) {
      lst0$contact_survey |>
        tibble::as_tibble() |>
        dplyr::summarise(value = sum(weight), .by = age_group) |>
        dplyr::mutate(type = "ns") |>
        dplyr::bind_rows(
          lst0$contact_survey_shielding |>
            tibble::as_tibble() |>
            dplyr::summarise(value = sum(weight), .by = age_group) |>
            dplyr::mutate(type = "s")
        )
    }) |>
    purrr::reduce(function(acc, lst0) {
      acc |>
        dplyr::full_join(lst0 |> dplyr::rename(v = value)) |>
        dplyr::mutate(value = dplyr::coalesce(value, 0) + dplyr::coalesce(v, 0)) |>
        dplyr::select(-v)
    }) |>
    tidyr::pivot_wider(names_from = type, values_from = value) |>
    dplyr::mutate(k = dplyr::coalesce(s, 0), n = k + ns) |>
    dplyr::arrange(age_group) -> dat0

  data_lst[[1]]$ps_k <- dat0$k
  data_lst[[1]]$ps_n <- dat0$n
  data_lst
}

run_inference <- function(model_fn, parameters, explanatory_variables, exp_names, exp_pos, data_lst, nposterior, lbl = "", draft = F) {
  # draft <- T
  # library(targets)
  # tar_load(survey_dat)
  # tar_load(parameters)
  # tar_load(explanatory_variables)
  # tar_load(model_fn)
  # tar_read(data_lst) -> data_lst
  # exp_names <- c("offset", "school", "work", "visit", "temp")
  # # Should they be limited to positive values?
  # exp_pos <- c(FALSE, FALSE, FALSE, FALSE, FALSE)
  # lbl <- ""
  # stop()
  fn <- paste0("batch_cache_", paste0(exp_names, collapse = "_"), lbl, ".qs")
  cache_path <- "cache"
  batch_cache_fn <- file.path(cache_path, fn)
  batch_cache <- NULL
  if (file.exists(batch_cache_fn)) {
    batch_cache <- qs::qread(batch_cache_fn)
  }

  # tar_load(parameters)
  parameters$cohort_dat -> cohort_dat

  # tar_load(explanatory_variables)
  explanatory_lst <- explanatory_variables$lst

  Sys.setenv("PKG_CXXFLAGS" = "-Wno-deprecated-enum-enum-conversion -Wno-ignored-attributes -Wno-deprecated-declarations -std=c++20")
  print("abcde")
  Rcpp::sourceCpp(file = model_fn, rebuild = T)

  # socialmixr::contact_matrix(survey_lst, symmetric = T, countries = "United Kingdom", age.limits = c(0, age_limits)) -> cm_lst
  # apply(cm_lst$m, 2, function(v) v/cm_lst$demography$population)
  # cm_pars <- cm_to_par(mean_cm_from_survey(data_lst[[4]]$contact_survey, cohort_dat$count))
  # #data_lst[[4]]$contact_survey -> dat0
  # pars <- c(cm_pars, rep(1.0, 9))
  # pars <- rprior_cpp(5)
  # batch_smc(1, pars, data_lst, cohort_dat$count)
  # batch_smc(1000, batch[1:2,], data_cpp, cohort_dat$count)

  prior_ptr <- prior_pointer(length(exp_names), exp_pos)
  npars <- ncol(rprior_cpp(2, prior_ptr))

  rprior_r <- function(n = 1) {
    rprior_cpp(n, prior_ptr)
  }
  dprior_r <- function(params) {
    dprior_cpp(params, prior_ptr)
  }

  init_batch <- if (is.null(batch_cache) || ncol(batch_cache) != npars) {
    # Fit to first data point only to get a good starting position (rprior)
    prior <- createPrior(density = dprior_r, sampler = rprior_r)
    init_data_lst <- list(`1` = data_lst[[1]])
    init_data_cpp <- data_pointer(init_data_lst, explanatory_lst)
    params <- rprior_cpp(1000000, prior_ptr)
    weights <- batch_smc(1, params, init_data_cpp, cohort_dat$count)
    tibble::tibble(w = weights) |>
      dplyr::mutate(i = dplyr::row_number()) |>
      dplyr::arrange(desc(w)) |>
      dplyr::pull(i) -> i
    # Best 100 samples
    params[i[1:100], ]
  } else {
    message("loading previous batch")
    batch_cache
  }

  rprior_init <- function(n = 1) {
    v <- seq(1, nrow(init_batch))
    v <- sample(v, n, replace = T)
    matrix(init_batch[v, 1:npars], nrow = n, ncol = npars)
  }
  prior_init <- createPrior(density = dprior_r, sampler = rprior_init)

  data_cpp <- data_pointer(data_lst, explanatory_lst)

  bayesianSetup <- createBayesianSetup(function(params) {
    if (!("matrix" %in% class(params))) {
      params <- matrix(params, nrow = 1)
    }
    batch_smc(1, params, data_cpp, cohort_dat$count)
  }, prior = prior_init, parallel = "external")

  nsamples <- 200000
  nParallel <- 10
  nstart <- nsamples * 0.5

  if (draft) {
    nsamples <- 20000
    nParallel <- 10
    nstart <- nsamples * 0.5
  }

  capture.output({
    out <- runMCMC(bayesianSetup,
      sampler = "DEzs",
      # sampler = "SMC",
      # settings =  list(iterations = 500))
      settings = list(
        iterations = nParallel * nsamples, startValue = nParallel,
        burnin = nstart, consoleUpdates = nParallel * nsamples / 50
      )
    )
    # eps = 1e-9, f = 0.1,
    # blockUpdate = list("user", groups = gps))) #, message = F))
  })

  getSample(out, numSamples = nposterior, start = nstart, parametersOnly = F) -> raw_batch
  batch <- raw_batch[, 1:(ncol(raw_batch) - 3)]
  qs::qsave(batch, file = batch_cache_fn)
  list(batch = batch, data_lst = data_lst, explanatory_lst = explanatory_lst)
}

archive_output <- function(model_fn, inference_result, parameters, explanatory_variables, lbl) {
  # library(grates)
  # library(tidyverse)
  # targets::tar_load(parameters)
  # targets::tar_load(model_fn)
  # targets::tar_load(explanatory_variables)
  # inference_result <- NULL
  # targets::tar_read(ns_inference_result) -> inference_result
  # lbl <- "ns_"
  # stop()
  dir_path <- "output"

  # Check if directory exists
  if (!dir.exists(dir_path)) {
    dir.create(dir_path)
  }

  # Create a new time_dat
  explanatory_variables$dat |>
    dplyr::select(period) |>
    dplyr::distinct() |>
    dplyr::arrange(period) |>
    dplyr::mutate(time_id = dplyr::row_number()) -> forward_time_dat

  # Create explanatory_lst using the new time_dat
  explanatory_variables$dat |>
    dplyr::inner_join(forward_time_dat) |>
    dplyr::ungroup() |>
    dplyr::arrange(time_id, activity) %>%
    split(., .$time_id) |>
    purrr::map(function(dt) dt$value) -> forward_explanatory_lst

  age_limits <- parameters$age_limits
  parameters$cohort_dat -> cohort_dat

  # TODO: check if this is still relevant
  # stopifnot(is.null(survey_dat$survey_lst$participants$time_id))

  forward_time_dat$time_id |>
    unique() |>
    purrr::map(function(t_id) {
      # Not used, so dummy data, but do use some data otherwise it will get confused
      inference_result$data_lst[[10]]
    }) -> forward_data_lst

  batch <- inference_result$batch
  v <- seq(1, nrow(batch)) |> sample(size = 250)
  batch[v, ] -> batch

  Sys.setenv("PKG_CXXFLAGS" = "-Wno-deprecated-enum-enum-conversion -Wno-ignored-attributes -Wno-deprecated-declarations -std=c++20")
  Rcpp::sourceCpp(file = model_fn, rebuild = T)
  forward_data_cpp <- data_pointer(forward_data_lst, forward_explanatory_lst)
  batch_smc_history(1, batch, forward_data_cpp, cohort_dat$count) -> forward_results_lst

  forward_results_lst |>
    purrr::imap(function(lst0, np_id) {
      lst0$particles |>
        purrr::imap(function(lst1, time_id) {
          t_dat <- forward_time_dat |> dplyr::filter(time_id == !!time_id)
          list(
            c_ij = par_to_cm(lst1$cm_pars),
            co_ij = par_to_co(lst1$cm_pars, batch[as.numeric(np_id), ]),
            cs_ij = par_to_cs(lst1$cm_pars, batch[as.numeric(np_id), ]),
            ps_i = par_to_ps(batch[as.numeric(np_id), ]),
            period = t_dat$period,
            period_lbl = as.character(t_dat$period)
          )
        })
    }) -> lst
  qs::qsave(lst, file.path(dir_path, paste0(lbl, "matrices.qs")))
  list(clean = lst, raw = forward_results_lst)
}


run_forward_modelling <- function(model_fn, data_lst, survey_dat, explanatory_variables, parameters, inference_result, mx_date) {
  # Forward modelling
  # tar_read(ms_model_fn) -> model_fn
  # tar_read(ms_inference_result) -> inference_result
  # tar_read(ms_data_lst) -> data_lst
  # tar_load(parameters)
  # tar_load(explanatory_variables)
  # mx_date <- lubridate::as_date("2022-07-01")
  #   stop()

  Sys.setenv("PKG_CXXFLAGS" = "-Wno-deprecated-enum-enum-conversion -Wno-ignored-attributes -Wno-deprecated-declarations -std=c++20")
  Rcpp::sourceCpp(file = model_fn, rebuild = T)

  v <- seq(1, nrow(inference_result$batch))
  v <- sample(v, size = 100)
  batch <- inference_result$batch[v, ]
  parameters$cohort_dat -> cohort_dat
  explanatory_dat <- explanatory_variables$dat

  # Create a new time_dat
  explanatory_dat |>
    dplyr::select(period) |>
    dplyr::distinct() |>
    dplyr::arrange(period) |>
    dplyr::mutate(time_id = dplyr::row_number()) |>
    dplyr::filter(as_date(period) < mx_date) -> forward_time_dat

  # Create explanatory_lst using the new time_dat
  explanatory_dat |>
    dplyr::inner_join(forward_time_dat) |>
    dplyr::ungroup() |>
    dplyr::arrange(time_id, activity) %>%
    split(., .$time_id) |>
    purrr::map(function(dt) dt$value) -> forward_explanatory_lst

  age_limits <- parameters$age_limits

  ageutils::breaks_to_interval(age_limits) |>
    dplyr::mutate(ag_id = dplyr::row_number()) -> age_map

  forward_time_dat$time_id |>
    unique() |>
    purrr::map(function(t_id) {
      # Not used, so dummy data, but do use some data otherwise it will get confused
      data_lst[[10]]
    }) -> forward_data_lst

  data_cpp <- data_pointer(forward_data_lst, forward_explanatory_lst)
  batch_smc_history(1, batch, data_cpp, cohort_dat$count) -> lst

  # Adjust time_id
  mn <- min(survey_dat$time_dat$period)
  forward_time_dat |>
    dplyr::filter(period == mn) |>
    dplyr::pull(time_id) -> v
  stopifnot(length(v) == 1)
  dt <- v - (data_lst[[1]]$time_id)
  data_lst |>
    purrr::map(function(lst0) {
      lst0$time_id <- lst0$time_id + dt
      lst0
    }) -> d_lst

  list(
    forward = lst,
    data_lst = d_lst, explanatory_lst = forward_explanatory_lst, age_map = age_map,
    batch = inference_result$batch, sample_id = v,
    time_dat = forward_time_dat
  )
}
