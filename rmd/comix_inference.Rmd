---
title: "Comix inference"
date: "`r Sys.Date()`"
author: Edwin van Leeuwen
params:
  shielding: false 
output:
  bookdown::html_document2: 
    toc: false
    number_sections: false
bibliography: references.bib
---

```{r setup, include=F}
library(grates)
library(targets)
library(tidyverse)
knitr::opts_chunk$set(cache = F, echo = F, message = F, warning = F, cache.lazy = F)

theme_set(theme_bw())
```

We have a custom contact model for comix, which has a base contact matrix $\hat{c}_{ij}$, and bi-weekly adjustment rates by age group $a_i$. The contact matrix is then defined as $$c_{ij} = \frac{a_i + a_j}{2} \hat{c}_{ij}$$. We assume $a_i$ depends on Google mobility and school attendance data sets linearly, e.g.: $a_i = \beta_0 + \beta_1 x_1 + \dots$ ($\beta_i$ is assumed to be positive in all cases).

# Model

## Splitting into shielding and non-shielding groups

Now let's split it into two groups, e.g. shielders ($s$) and others ($o$) and allow them to change their contact rate $c^s_{ij}$ and $c^o_{ij}$.

$$\begin{aligned}
c_{ij}N_j &= (c^s_{ij} p^s_j + c^o_{ij} (1-p^s_j)) N_j\\
c_{ij} &= (c^s_{ij} p^s_j + c^o_{ij} (1-p^s_j))\\
\end{aligned}$$

Except for the above constraint, we will also assume that shielding participants will reduce their contacts regardless of the contacts age group ($c^s_{ij}=\alpha_i c^o_{ij}$). This allows us to calculate the contact rates within for the shielders and non-shielders as dependent on $\alpha_i$ and $p^s_i$:

$$\begin{aligned}
c^s_{ij} & = \frac{\alpha_i c_{ij}}{(a_i -1)p^s_j + 1}\\
c^o_{ij} & = \frac{c_{ij}}{(\alpha_i -1)p^s_j + 1}\\
\end{aligned}$$

Note when the fraction of shielders is zero this simplifies to $c^o_{ij} = c_{ij}$, and we might want to use the $c_{ij}$ matrix before the first lockdown.

### For discussion:

- Should the behaviour ($\alpha_i$) and the fraction of shielders $p^s_i$ be time dependent and if so, which explanatory variables should they depend on? In theory, it would be relatively straightforward to fit $p^s_i$ to the number of shielders in the contact matrix over time.

## Likelihood

We use shielding data from OpenSafely as input data.

From the fitting we can derive the number of contacts a for each cohort ($c^s_{ij}N_j$ and $c^o_{ij}N_j$). Contact survey data has the number of contacts different participants have with different age groups. We can then define the following likelihood for the participants as follows: 
$$\begin{aligned}
P(y^{s,k}_j; c^s_{k \mapsto i,j}N_j)\\
\mathcal{L}=\prod_\nu \prod_k\prod_j P(y^{\nu,k}_j; c^\nu_{k \mapsto i,j}N_j)
\end{aligned}$$
where $\nu \in {s, o}$ is the shielding state of the participant, $k$ is the participant, $i$ is the age group of participant $k$ and $j$ is the age group of the contact.


```{r prepareYourself}
tar_load(parameters)
tar_load(explanatory_variables)
tar_load(survey_dat)
tar_load(model_fn)
tar_load(inference_result)

v <- seq(1, nrow(inference_result$batch))
v <- sample(v, size = 100)
batch <- inference_result$batch[v, ]
data_lst <- inference_result$data_lst
# Technically could use the explanatory_variables target, but should have same outcomes
explanatory_lst <- inference_result$explanatory_lst

time_dat <- survey_dat$time_dat
survey_lst <- survey_dat$survey_lst

parameters$cohort_dat -> cohort_dat
explanatory_dat <- explanatory_variables$dat

Sys.setenv("PKG_CXXFLAGS" = "-Wno-deprecated-enum-enum-conversion -Wno-ignored-attributes -Wno-deprecated-declarations -std=c++20")
Rcpp::sourceCpp(file = model_fn, rebuild = T)
```

```{r wholeYear}
mx_date <- lubridate::as_date("2021-07-01")
# Create a new time_dat
explanatory_dat |>
  dplyr::select(period) |>
  dplyr::distinct() |>
  dplyr::arrange(period) |>
  dplyr::mutate(time_id = dplyr::row_number()) |>
  dplyr::filter(as_date(period) < mx_date) -> forward_time_dat

# Create explanatory_lst using the new time_dat
explanatory_dat |>
  dplyr::inner_join(forward_time_dat) |>
  dplyr::ungroup() |>
  dplyr::arrange(time_id, activity) %>%
  split(., .$time_id) |>
  purrr::map(function(dt) dt$value) -> forward_explanatory_lst

age_limits <- parameters$age_limits

ageutils::breaks_to_interval(age_limits) |>
  dplyr::mutate(ag_id = dplyr::row_number()) -> age_map

stopifnot(is.null(survey_lst$participants$time_id))

forward_time_dat$time_id |>
  unique() |>
  purrr::map(function(t_id) {
    # Not used, so dummy data, but do use some data otherwise it will get confused
    data_lst[[10]]
  }) -> forward_data_lst
```

```{r alphaOverTime, fig.height = 12, fig.cap = "Alpha over time by age group"}
data_cpp <- data_pointer(forward_data_lst, forward_explanatory_lst)
batch_smc_history(1, batch, data_cpp, cohort_dat$count) -> results_lst
# batch_smc_history(500, batch, data_lst, cohort_dat$count) -> results_lst
results_lst |>
  purrr::imap(function(lst0, np_id) {
    lst0$particles |>
      purrr::imap(function(lst1, time_id) {
        tibble::tibble(alpha = lst1$alpha) |>
          dplyr::mutate(
            ag_id = dplyr::row_number(),
            time_id = as.numeric(time_id)
          )
      }) |>
      dplyr::bind_rows() |>
      dplyr::mutate(np = as.numeric(np_id))
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::left_join(forward_time_dat) -> plot_dat

ggplot(data = plot_dat) +
  geom_line(aes(x = period, y = alpha, group = np), alpha = 0.25) +
  facet_grid(interval ~ ., scales = "free") +
  expand_limits(y = 0)
```

```{r dataAndFit, fig.height = 12, fig.cap = "Number of contacts over time by age group for the non-shielding cohort. Red is the data, black is the inferred value"}
# Use forward result lst to also show backward in time
results_lst |>
  purrr::imap(function(lst0, np_id) {
    lst0$particles |>
      purrr::imap(function(lst1, time_id) {
        m <- with_pop(par_to_co(lst1$cm_pars, batch[as.numeric(np_id), ]), cohort_dat$count)
        dplyr::tibble(rate = m |> rowSums(), np = np_id, time_id = !!time_id) |>
          dplyr::mutate(ag_id = 1:9)
      }) |>
      dplyr::bind_rows()
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::left_join(forward_time_dat) -> inference_dat

# Summarised in number of contacts over time
data_lst |>
  purrr::imap(function(lst0, time_id) {
    lst0$contact_survey |>
      as_tibble() |>
      dplyr::rename(ag_id = age_group) |>
      dplyr::summarise(rate = sum(value * weight) / sum(weight), weight = sum(weight), .by = c(ag_id, cnt_age_group)) |>
      dplyr::summarise(rate = sum(rate), weight = sum(weight), .by = c(ag_id)) |>
      dplyr::mutate(time_id = !!time_id)
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::left_join(time_dat) -> dat

ggplot(data = dat) +
  geom_line(data = inference_dat, aes(x = period, y = rate, group = np), alpha = 0.05) +
  geom_line(aes(x = period, y = rate, group = interval, alpha = weight), colour = "Red") +
  geom_point(aes(x = period, y = rate, group = interval, alpha = weight), colour = "Red") +
  expand_limits(y = 0) +
  facet_grid(interval ~ ., scales = "free")
```

```{r dataAndFitShield, fig.height = 12, fig.cap = "Number of contacts over time by age group for the shielding cohort. Red is the data, black is the inferred value"}
# Use forward result lst to also show backward in time
results_lst |>
  purrr::imap(function(lst0, np_id) {
    lst0$particles |>
      purrr::imap(function(lst1, time_id) {
        m <- with_pop(par_to_cs(lst1$cm_pars, batch[as.numeric(np_id), ]), cohort_dat$count)
        dplyr::tibble(rate = m |> rowSums(), np = np_id, time_id = !!time_id) |>
          dplyr::mutate(ag_id = 1:9)
      }) |>
      dplyr::bind_rows()
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::left_join(forward_time_dat) -> inference_dat

# Summarised in number of contacts over time
data_lst |>
  purrr::imap(function(lst0, time_id) {
    lst0$contact_survey_shielding |>
      as_tibble() |>
      dplyr::rename(ag_id = age_group) |>
      dplyr::summarise(rate = sum(value * weight) / sum(weight), weight = sum(weight), .by = c(ag_id, cnt_age_group)) |>
      dplyr::summarise(rate = sum(rate), weight = sum(weight), .by = c(ag_id)) |>
      dplyr::mutate(time_id = !!time_id)
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::left_join(time_dat) -> dat

ggplot(data = dat) +
  geom_line(data = inference_dat, aes(x = period, y = rate, group = np), alpha = 0.05) +
  geom_line(aes(x = period, y = rate, group = interval, alpha = weight), colour = "Red") +
  geom_point(aes(x = period, y = rate, group = interval, alpha = weight), colour = "Red") +
  expand_limits(y = 0) +
  facet_grid(interval ~ ., scales = "free")
```

```{r explanatoryOverTime, fig.cap = "Explanatory values over time"}
ggplot(data = explanatory_dat |> dplyr::filter(as_date(period) < mx_date)) +
  geom_line(aes(x = period, y = value)) +
  facet_grid(activity ~ ., scales = "free") +
  expand_limits(y = 0)
```

```{r evPlot, fig.cap = "Dominant eigenvalue over time", eval = F}
results_lst |>
  purrr::imap(function(lst0, np_id) {
    lst0$particles |>
      purrr::imap(function(lst1, time_id) {
        apply(par_to_cm(lst1$cm_pars), 2, function(v) v * cohort_dat$count) |>
          eigen(only.values = T) -> ev
        tibble(value = max(ev$values), time = as.numeric(time_id))
      }) |>
      dplyr::bind_rows() |>
      dplyr::mutate(np = as.numeric(np_id))
  }) |>
  dplyr::bind_rows() -> plot_dat

ggplot(data = plot_dat) +
  geom_line(aes(x = time, y = value, group = np), alpha = 0.25) +
  expand_limits(y = 0)
```

```{r linear, fig.cap = "Betas for the explanatory values by age group", fig.height = 8}
length(explanatory_lst[[1]]) -> nexp
nag <- length(age_limits)
npars <- ncol(batch)
batch[, (npars - nag * nexp + 1):npars] -> lbatch
lbatch |>
  tibble::as_tibble() |>
  dplyr::mutate(np = dplyr::row_number()) |>
  tidyr::pivot_longer(cols = -np) |>
  dplyr::group_by(np) |>
  dplyr::mutate(ag_id = cumsum(dplyr::row_number() %% nexp == 1)) |>
  dplyr::group_by(np, ag_id) |>
  dplyr::mutate(exp_id = dplyr::row_number(), exp_nm = exp_names) |>
  dplyr::ungroup() |>
  dplyr::left_join(age_map, by = c("ag_id")) -> lbatch_dat

ggplot(lbatch_dat) +
  geom_point(aes(x = interval, y = value, group = interval), position = position_dodge2(0.8)) +
  ylab("beta") +
  facet_grid(exp_nm ~ ., scales = "free")
```

```{r shield, fig.cap = "Relative contact rates shielders (alpha)", fig.height = 8}
seq(1, nrow(batch)) |>
  purrr::map(function(np_id) {
    tibble(value = par_to_srate(batch[as.numeric(np_id), ]), ag_id = 1:9) |>
      dplyr::mutate(np = np_id)
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::mutate(np = as.numeric(np)) -> dat

ggplot(dat) +
  expand_limits(y = c(0, 1)) +
  ylab("alpha") +
  geom_point(aes(x = interval, y = value, group = interval), position = position_dodge2(0.8))
```

```{r propShield, fig.cap = "Shielding proportion by age group (ps)", fig.height = 8}
seq(1, nrow(batch)) |>
  purrr::map(function(np_id) {
    tibble(value = par_to_ps(batch[as.numeric(np_id), ]), ag_id = 1:9) |>
      dplyr::mutate(np = np_id)
  }) |>
  dplyr::bind_rows() |>
  dplyr::left_join(age_map, by = c("ag_id")) |>
  dplyr::mutate(np = as.numeric(np)) -> dat
ggplot(dat) +
  ylab("Shielding proportion") +
  geom_point(aes(x = interval, y = value, group = interval), position = position_dodge2(0.8))
```

## Parameter fit

```{r parameters, fig.height = 28}
batch |>
  dplyr::as_tibble() |>
  tidyr::gather(key, value) -> plot_dat

ggplot(plot_dat) +
  geom_histogram(aes(x = value)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  facet_wrap(key ~ ., ncol = 5, scales = "free")
```

```{r likelihood, fig.cap = "likelihood", eval = F}
tibble::tibble(value = raw_batch[, ncol(raw_batch) - 2]) |>
  dplyr::mutate(np = dplyr::row_number()) -> plot_dat

ggplot(plot_dat) +
  geom_point(aes(x = np, y = value))
```

# References
