// [[Rcpp::depends(simpleRSMC)]]
// [[Rcpp::depends(Rcpp)]]
// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppParallel)]]
#include <Rcpp.h>
#include <RcppEigen.h>
#include <RcppParallel.h>
#undef dnorm
#undef dunif

#include "distribution.hpp"
#include "parallel.hpp"
#include "smc.hpp"

constexpr int nag = 9;

#include "contact_model.hpp"

// High contact relative rates
constexpr int nhc_b = contacts::nparameters;
constexpr int nhc = nag;
// Prop of being high contact
constexpr int nps_b = nhc_b + nhc;
constexpr int nps = nag;
// Contact changes over time
constexpr int nexp_b = nps_b + nps;

struct State {
  // Note that this is constant, so bit of a waste to copy over
  Eigen::Array<double, contacts::nparameters, 1> cm_pars;

  Eigen::Array<double, nag, 1> alpha;

  size_t time = 0;
};

struct Data {
  Eigen::MatrixXd contact_survey;
};

namespace Rcpp {
template <> SEXP wrap(const State &state) {
  Rcpp::List RState(0);
  RState["cm_pars"] = state.cm_pars;
  RState["alpha"] = state.alpha;
  RState["time"] = state.time;
  return Rcpp::wrap(RState);
}

template <> SEXP wrap(const Data &data) {
  Rcpp::List RData(0);
  RData["contact_survey"] = data.contact_survey;
  return Rcpp::wrap(RData);
}

template <> Data as(SEXP dataRObj) {
  Data data;
  auto RData = Rcpp::as<Rcpp::List>(dataRObj);

  if (RData.containsElementNamed("contact_survey"))
    data.contact_survey = RData["contact_survey"];

  return data;
}
} // namespace Rcpp

struct prior_share {
  size_t nexplanatory = 0;
  size_t npars = 0;
  std::vector<bool> explanatory_positive;
};

// [[Rcpp::export]]
auto prior_pointer(size_t nexplanatory, std::vector<bool> exp_pos) {
  auto *ps = new prior_share();
  ps->nexplanatory = nexplanatory;
  ps->npars = nexp_b + nexplanatory * nag;
  ps->explanatory_positive = exp_pos;
  if (exp_pos.size() != nexplanatory) {
    Rcpp::stop("Mismatched size for the explanatory variables");
  }

  Rcpp::XPtr<prior_share> ptr(ps, true);
  return ptr;
}

struct DataShare {
  std::vector<Data> data;
  std::vector<int> times;
  std::vector<Eigen::ArrayXd> explanatory;
};

// [[Rcpp::export]]
auto data_pointer(const std::vector<Data> &data,
                  const std::vector<Eigen::ArrayXd> &explanatory) {
  auto *pd = new DataShare();
  pd->data.resize(data.size());
  for (auto i = 0; i < data.size(); ++i) {
    pd->data[i] = data[i];
  }

  pd->explanatory.resize(explanatory.size());
  for (auto i = 0; i < explanatory.size(); ++i) {
    pd->explanatory[i] = explanatory[i];
  }

  pd->times.resize(data.size());
  std::iota(pd->times.begin(), pd->times.end(), 1);

  Rcpp::XPtr<DataShare> ptr(pd, true);
  return ptr;
}

double rlinear(const Eigen::ArrayXd &params, const Eigen::ArrayXd &vars,
               double base = 1.0) {
  double value = base;
  for (auto i = 0; i < vars.size(); ++i) {
    value += vars(i) * params[i];
  }
  return value;
}

smc::Model<State, Data, Eigen::ArrayXd>
build_model(const Eigen::ArrayXd &demography,
            const std::vector<Eigen::ArrayXd> &explanatory,
            std::default_random_engine &generator) {
  smc::Model<State, Data, Eigen::ArrayXd> model;

  model.rinit = [&explanatory](const Eigen::ArrayXd &params) {
    State state;

    state.cm_pars = params.block(0, 0, contacts::nparameters, 1);

    auto exps = explanatory[0];

    for (auto i = 0; i < nag; ++i) {
      state.alpha(i) = rlinear(
          params.block(nexp_b + i * exps.size(), 0, exps.size(), 1), exps);
    }

    return state;
  };

  model.rprocess = [&explanatory, &generator](State &&state,
                                              const Eigen::ArrayXd &params) {
    auto exps = explanatory[state.time];
    for (auto i = 0; i < nag; ++i) {
      state.alpha(i) = rlinear(
          params.block(nexp_b + i * exps.size(), 0, exps.size(), 1), exps);
    }

    for (size_t i = 0; i < contacts::nparameters; ++i) {
      auto &[k, l] = contacts::par_cm_indices[i];
      state.cm_pars(i) = 0.5 * (state.alpha(k) + state.alpha(l)) * params[i];
      // state.cm_pars(i) = (state.alpha(k) * state.alpha(l)) * params[i];
    }

    ++state.time;
    return std::move(state);
  };

  model.dmeasure = [&demography, &generator](const Data &data,
                                             const State &state,
                                             const Eigen::ArrayXd &params) {
    static thread_local Eigen::Matrix<double, nag, nag> cm;
    static thread_local Eigen::Matrix<double, nag, nag> cs;
    static thread_local Eigen::Matrix<double, nag, nag> co;
    static thread_local Eigen::Array<double, 2, 1> lls;

    double ll = 0;
    int dummy = -1;

    // Check values are (at least) positive
    for (size_t i = 0; i < state.cm_pars.size(); ++i) {
      if (state.cm_pars.size() < 0)
        return log(0);
    }

    // Do the matrix
    auto as = params.segment(nhc_b, nhc);
    auto ps = params.segment(nps_b, nps);

    cm = contacts::par_to_cm(state.cm_pars);
    for (auto i = 0; i < cm.rows(); ++i) {
      for (auto j = 0; j < cm.cols(); ++j) {
        co(i, j) = demography(j) * cm(i, j) / ((as(i) - 1) * ps[j] + 1);
        if (co(i, j) < 0)
          return log(0);
        cs(i, j) = as(i) * co(i, j);
      }
    }

    for (auto k = 0; k < data.contact_survey.rows(); ++k) {
      // age group id starts at 1
      auto i = data.contact_survey(k, 0) - 1;
      // We assume the person is part of one or the other?
      // TODO: Should this be weight by the number of age groups, i.e. once for
      // each?
      lls(0) = log(1.0 - ps(i));
      lls(1) = log(ps(i));
      for (auto j = 0; j < cm.cols(); ++j) {
        double ll_temp = 0;
        distribution::unified_interface<distribution::Poisson, false>(
            ll_temp, data.contact_survey(k, j + 2), generator, co(i, j));
        lls(0) += ll_temp;

        ll_temp = 0;
        distribution::unified_interface<distribution::Poisson, false>(
            ll_temp, data.contact_survey(k, j + 2), generator, cs(i, j));
        lls(1) += ll_temp;
      }
      // lls(0) or lls(1), summed and multiplied by number of people with this
      // contact pattern
      ll += smc::log_sum_exponentials(lls) * data.contact_survey(k, 1);
    }
    return ll;
  };

  model.rmeasure = [&demography,
                    &generator](const State &state,
                                const Eigen::ArrayXd &params) -> Data {
    static thread_local Eigen::Matrix<double, nag, nag> cm;
    static thread_local Eigen::Matrix<double, nag, nag> cs;
    static thread_local Eigen::Matrix<double, nag, nag> co;

    Data data_point;
    // Do the matrix
    auto as = params.segment(nhc_b, nhc);
    auto ps = params.segment(nps_b, nps);

    cm = contacts::par_to_cm(state.cm_pars);
    for (auto i = 0; i < cm.rows(); ++i) {
      for (auto j = 0; j < cm.cols(); ++j) {
        co(i, j) = demography(j) * cm(i, j) / ((as(i) - 1) * ps[j] + 1);
        cs(i, j) = as(i) * co(i, j);
      }
    }
    // One random draw for each age group
    data_point.contact_survey = Eigen::MatrixXd::Zero(cm.rows(), cm.cols() + 2);
    for (auto k = 0; k < data_point.contact_survey.rows(); ++k) {
      // age group
      auto i = k;
      // age group id starts at 1
      data_point.contact_survey(k, 0) = k + 1;
      data_point.contact_survey(k, 1) = 1;
      for (auto j = 0; j < cm.cols(); ++j) {
        double ll_temp = 0;
        if (distribution::runif(0, 1, generator) < 1.0 - ps(i)) {
          distribution::unified_interface<distribution::Poisson, true>(
              ll_temp, data_point.contact_survey(k, j + 2), generator,
              co(i, j));
        } else {
          distribution::unified_interface<distribution::Poisson, true>(
              ll_temp, data_point.contact_survey(k, j + 2), generator,
              cs(i, j));
        }
      }
    }
    return data_point;
  };
  return model;
}

template <bool draw>
void prior(double &ll, Eigen::Ref<Eigen::MatrixXd> params,
           const Rcpp::XPtr<prior_share> &ptr,
           std::default_random_engine &generator) {
  for (auto i = 0; i < params.rows(); ++i) {
    for (auto j = 0; j < contacts::nparameters; ++j) {
      if constexpr (draw) {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j), generator, 1e-9, 1e-6);
      } else {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j), generator, 0.0, 1e-4);
      }
    }
    for (auto j = 0; j < nag * ptr->nexplanatory; ++j) {
      if constexpr (draw) {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j + nexp_b), generator, 0.0, 0.01);
      } else {
        int id = j % ptr->nexplanatory;
        if (ptr->explanatory_positive[id]) {
          distribution::unified_interface<distribution::Uniform, draw>(
              ll, params(i, j + nexp_b), generator, 0.0, 100.0);
        } else {
          distribution::unified_interface<distribution::Uniform, draw>(
              ll, params(i, j + nexp_b), generator, -100.0, 100.0);
        }
      }
    }
    for (auto j = 0; j < nhc; ++j) {
      if constexpr (draw) {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j + nhc_b), generator, 1.0, 1.10);
      } else {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j + nhc_b), generator, 1.0, 50.0);
      }
    }
    for (auto j = 0; j < nps; ++j) {
      if constexpr (draw) {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j + nps_b), generator, 0.0, 0.3);
      } else {
        distribution::unified_interface<distribution::Uniform, draw>(
            ll, params(i, j + nps_b), generator, 0.0, 1.0);
      }
    }
  }
}

// [[Rcpp::export]]
Eigen::MatrixXd rprior_cpp(size_t n, const Rcpp::XPtr<prior_share> &ptr) {
  static thread_local std::atomic<size_t> seed = 0;
  std::default_random_engine generator(++seed + time(0));
  Eigen::MatrixXd params(n, ptr->npars);
  params.fill(-1);
  double ll = 0;
  prior<true>(ll, params, ptr, generator);
  return params;
}

// [[Rcpp::export]]
auto dprior_cpp(const Eigen::ArrayXd &params,
                const Rcpp::XPtr<prior_share> &ptr) {
  std::default_random_engine generator(time(0));
  Eigen::MatrixXd m(1, params.size());
  m.row(0) = params;
  double ll = 0;
  prior<false>(ll, m, ptr, generator);
  return ll;
}

// [[Rcpp::export]]
auto batch_smc(const size_t nparticles, const Eigen::MatrixXd &params,
               const Rcpp::XPtr<DataShare> &data_ptr,
               const Eigen::ArrayXd &demography) {
  const std::vector<Data> &data = data_ptr.get()->data;
  const std::vector<int> &times = data_ptr.get()->times;
  const std::vector<Eigen::ArrayXd> &explanatory = data_ptr.get()->explanatory;

  auto build_f = [&demography,
                  &explanatory](std::default_random_engine &generator) {
    return build_model(demography, explanatory, generator);
  };
  /*std::vector<int> times(data.size());
    std::iota(times.begin(), times.end(), 1);*/
  return smc::batch_smc<State>(nparticles, params, data, times, 0, build_f);
}

// [[Rcpp::export]]
auto batch_smc_history(const size_t nparticles, const Eigen::MatrixXd &params,
                       const Rcpp::XPtr<DataShare> &data_ptr,
                       const Eigen::ArrayXd &demography) {
  const std::vector<Data> &data = data_ptr.get()->data;
  const std::vector<int> &times = data_ptr.get()->times;
  const std::vector<Eigen::ArrayXd> &explanatory = data_ptr.get()->explanatory;

  auto build_f = [&demography,
                  &explanatory](std::default_random_engine &generator) {
    return build_model(demography, explanatory, generator);
  };

  /*std::vector<int> times(data.size());
    std::iota(times.begin(), times.end(), 1);*/
  return smc::batch_smc_history<State>(nparticles, params, data, times, 0,
                                       build_f);
}

// [[Rcpp::export]]
auto cm_to_par(const Eigen::MatrixXd &m) { return contacts::cm_to_par(m); }

// Calculates an estimated contact matrix from the survey. Useful to calculate
// "starting" matrix
auto mean_cm_from_survey(const Eigen::MatrixXd &survey,
                         const Eigen::ArrayXd &demography) {

  // BROKEN NOW. USES THE OLD DATA LAYOUT

  Eigen::Matrix<double, nag, nag> cm;
  cm.fill(0);
  Eigen::Array<double, nag, 1> participants;
  participants.fill(0);

  for (auto k = 0; k < survey.rows(); ++k) {
    // age group id starts at 1
    auto i = survey(k, 0) - 1;

    for (auto j = 0; j < nag; ++j) {
      cm(i, j) += survey(k, j + 1);
    }
    ++participants(i);
  }

  for (auto i = 0; i < nag; ++i)
    cm.row(i) /= participants(i);

  // Bring it in line with Petra's notation for fun and profit
  cm.transposeInPlace();

  for (size_t i = 0; i < nag; ++i) {
    for (size_t j = 0; j <= i; ++j) {
      double c = 0.5 * (cm(i, j) / demography(i) + cm(j, i) / demography(j));
      cm(i, j) = c;
      cm(j, i) = c;
    }
  }

  return cm;
}

// [[Rcpp::export]]
auto with_pop(const Eigen::MatrixXd &m, const Eigen::ArrayXd &pop) {
  // For future: m %*% diag(v)
  Eigen::MatrixXd res = m;
  for (auto i = 0; i < m.rows(); ++i) {
    for (auto j = 0; j < m.cols(); ++j) {
      res(i, j) = res(i, j) * pop(j);
    }
  }
  return res;
}

// [[Rcpp::export]]
auto par_to_cm(const Eigen::ArrayXd &params) {
  return contacts::par_to_cm(params);
}

// [[Rcpp::export]]
auto par_to_cs(const Eigen::ArrayXd &cm_pars, const Eigen::ArrayXd &params) {
  Eigen::Matrix<double, nag, nag> cm;
  Eigen::Matrix<double, nag, nag> cs;
  Eigen::ArrayXd as = params.block(nhc_b, 0, nhc, 1);
  Eigen::ArrayXd ps = params.block(nps_b, 0, nps, 1);

  cm = contacts::par_to_cm(cm_pars);
  for (auto i = 0; i < cm.rows(); ++i) {
    for (auto j = 0; j < cm.cols(); ++j) {
      cs(i, j) = as(i) * cm(i, j) / ((as(i) - 1) * ps(j) + 1);
      // co(i, j) = cm(i, j) / ((as(i) - 1) * ps(j) + 1);
    }
  }

  return cs;
}

// [[Rcpp::export]]
auto par_to_co(const Eigen::ArrayXd &cm_pars, const Eigen::ArrayXd &params) {
  Eigen::Matrix<double, nag, nag> cm;
  Eigen::Matrix<double, nag, nag> co;
  Eigen::ArrayXd as = params.block(nhc_b, 0, nhc, 1);
  Eigen::ArrayXd ps = params.block(nps_b, 0, nps, 1);

  cm = contacts::par_to_cm(cm_pars);
  for (auto i = 0; i < cm.rows(); ++i) {
    for (auto j = 0; j < cm.cols(); ++j) {
      // cs(i, j) = as(i) * cm(i, j) / ((as(i) - 1) * ps(j) + 1);
      co(i, j) = cm(i, j) / ((as(i) - 1) * ps(j) + 1);
    }
  }

  return co;
}

// [[Rcpp::export]]
auto par_to_as(const Eigen::ArrayXd &params) {
  return params.segment(nhc_b, nhc);
}

// [[Rcpp::export]]
auto par_to_ps(const Eigen::ArrayXd &params) {
  return params.segment(nps_b, nps);
}
