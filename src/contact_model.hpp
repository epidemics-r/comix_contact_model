#pragma once

// [[Rcpp::depends(simpleRSMC)]]
// [[Rcpp::depends(RcppEigen)]]
// [[Rcpp::depends(RcppParallel)]]
#include <Rcpp.h>
#include <RcppEigen.h>

#include "distribution.hpp"

namespace contacts {
constexpr int nparameters = []() {
  int s = 0;
  int n = nag;
  while (n > 0) {
    s += n;
    --n;
  }
  return s;
}();

// Map the parameter indices to matrix indices
constexpr auto par_cm_indices = []() {
  std::array<std::pair<int, int>, nparameters> indices;

  int k = 0;
  for (int i = 0; i < nag; ++i) {
    for (int j = 0; j <= i; ++j) {
      indices[k] = {i, j};
      ++k;
    }
  }
  return indices;
}();

auto cm_to_par(const Eigen::MatrixXd &m) {
  static thread_local Eigen::Array<double, nparameters, 1> params;
  for (auto i = 0; i < nparameters; ++i) {
    auto &[k, l] = par_cm_indices[i];
    params[i] = m(k, l);
  }
  return params;
}

auto par_to_cm(const Eigen::Ref<const Eigen::ArrayXd> params) {
  static thread_local Eigen::Matrix<double, nag, nag> m;
  for (auto i = 0; i < nparameters; ++i) {
    auto &[k, l] = par_cm_indices[i];
    m(k, l) = params[i];

    if (l != k)
      m(l, k) = params[i];
  }
  return m;
}

template <bool draw, typename D>
void measure(double &ll, D &survey, std::default_random_engine &generator,
             const Eigen::Ref<const Eigen::ArrayXd> params,
             const Eigen::ArrayXd &demography) {
  static thread_local Eigen::Matrix<double, nag, nag> cm;
  if (survey.rows() > 0) {
    cm = contacts::par_to_cm(params);
    for (auto k = 0; k < survey.rows(); ++k) {
      // age group id starts at 1
      auto i = survey(k, 0) - 1;
      auto j = survey(k, 1) - 1;

      double ll_temp = 0;
      distribution::unified_interface<distribution::Binomial, draw>(
          ll_temp, survey(k, 2), generator, demography(j), cm(i, j));
      ll += survey(k, 3) * ll_temp;
    }
  }
}

// Assuming poisson uncertainty, should be relatively approximately the same as
// the binomial in most cases and is significantly faster
template <bool draw, typename D>
void measure_poisson(double &ll, D &survey,
                     std::default_random_engine &generator,
                     const Eigen::Ref<const Eigen::ArrayXd> params,
                     const Eigen::ArrayXd &demography) {
  static thread_local Eigen::Matrix<double, nag, nag> cm;
  // Check values are (at least) positive
  if constexpr (!draw) {
    for (size_t i = 0; i < nparameters; ++i) {
      if (params(i) < 0)
        ll += log(0);
    }
  }

  if (survey.rows() > 0) {
    cm = contacts::par_to_cm(params);
    for (auto k = 0; k < survey.rows(); ++k) {
      // age group id starts at 1
      auto i = survey(k, 0) - 1;
      auto j = survey(k, 1) - 1;

      double ll_temp = 0;
      distribution::unified_interface<distribution::Poisson, draw>(
          ll_temp, survey(k, 2), generator, demography(j) * cm(i, j));
      ll += survey(k, 3) * ll_temp;
    }
  }
}

auto dmeasure(const Eigen::ArrayXd &params, Eigen::MatrixXd survey,
              const Eigen::ArrayXd &demography) {
  std::default_random_engine generator(time(0));
  double ll = 0;
  measure<false>(ll, survey, generator, params, demography);
  return ll;
}

auto rmeasure(const Eigen::ArrayXd &params, Eigen::MatrixXd survey,
              const Eigen::ArrayXd &demography) {
  std::default_random_engine generator(time(0));
  double ll = 0;
  measure<true>(ll, survey, generator, params, demography);
  return survey;
}
} // namespace contacts
